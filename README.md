# Hestia Child Theme
Tema child per fare pratica con WP QUery e la gerarchia dei template

## Features:
* Custom Post Type: Formazione & Eventi
* Formato dell'immagine personalizzato

## Requirements
* PHP
* WordPress
* Hestia Theme https://it.wordpress.org/themes/hestia/
* Plugin CPTUI https://it.wordpress.org/plugins/custom-post-type-ui/
* Crea un custom post type 'formazione' e un custom post type 'eventi'
* Inserisci dei contenuti all'interno dei CPT creati

## Installation
1. Installa il tema genitore Hestia
2. Carica la cartella hestia-child nella directory dei temi wp-content/themes
3. Vai nel tuo backend WP admin
4. Vai su "Apparenza -> Temi"
5. Attiva il tema figlio Hestia Child
