<?php
/**
 * The front page template file.
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hestia
 * @since Hestia 1.0
 */
get_header();?>

	<?php  //Vars
	$heroSubtitle = get_field('hero_subtitle');
	?>
	<header class="hero-header" style="background: linear-gradient(0deg, rgba(2,0,36,.8) 0%, rgba(57,57,57,.6) 51%, rgba(0,0,0,0) 100%), url('<?php the_field('hero_image'); ?>');">
		<div class="hero-text">
			<h1> <?php the_field('hero_title'); ?> </h1>
			<?php if ( !empty($heroSubtitle) ): ?>
				<p> <?php echo $heroSubtitle; ?> </p>
			<?php endif; ?>
		</div>
	</header>


		<?php //Display Latest event posts [max: 2]
		$args = array(
				'post_type'		=> 'eventi',
				'posts_per_page' => 2
				);

		 $query = new WP_Query($args);
		 // Start Loop
		 if ( $query->have_posts() ) : ?>

				<section id="latest-events" class="container-fluid vertical-spacing">
					<div class="row">
						<div class="col-xs-12 mb-4 title-section">
							<h2>EVENTI & PROGETTI</h2>
							<a href="<?php echo get_post_type_archive_link('eventi'); ?>">TUTTI GLI EVENTI</a>
						</div>
					</div>

					<?php while ( $query->have_posts() ) : $query->the_post();?>
	          <a href="<?php the_permalink(); ?>" class="events--item">
							<div class="col-md-6">
								<div class="row">
									<div class="col-xs-4 events--image">
										<?php
										if ( has_post_thumbnail() ){
											the_post_thumbnail( 'events-thumb', array('class' => 'img-responsive') );
										} else{
											echo '<img src="'.get_stylesheet_directory_uri().'/assets/images/placeholder-events.jpg" alt="placeholder immagine evento">';
										}?>
									</div>
									<div class="col-xs-8 events--text">
										<h3><?php the_title(); ?></h3>
										<p> <?php the_excerpt(); ?> </p>
										<!-- // wp_trim_excerpt() -->
									</div>
								</div>
							</div>
						</a>
	        <?php endwhile; ?>
			</section>
		<?php
		endif;
		wp_reset_postdata();
		//END Latest post?>

		<?php if ( have_posts() ) : ?>
			<section class="container-fluid">
				<div class="row">
					<div class="col-xs-12">
						<?php   // Start main loop
							while ( have_posts() ) :
								the_post();?>

								<?php the_content(); ?>

							<?php endwhile; ?>
						</div>
					</div>
			</section>
		<?php endif; ?>


		<?php //Display Latest event posts [max: 2]
		$args = array(
				'post_type'		=> 'formazione',
				'posts_per_page' => 4
				);

		 $query = new WP_Query($args);
		 // Start Loop
		 if ( $query->have_posts() ) : ?>

				<section id="latest-formazione" class="container-fluid vertical-spacing">
					<div class="row">
						<div class="col-xs-12 mb-4 title-section title-alternative">
							<h2>FORMAZIONE</h2>
							<a href="<?php echo get_post_type_archive_link('formazione'); ?>" class="u-upp">
								VEDI TUTTE LE PROPOSTE DI FORMAZIONE
							</a>
						</div>
					</div>

					<?php while ( $query->have_posts() ) : $query->the_post();?>
	          <a href="<?php the_permalink(); ?>" class="formazione--item">
							<div class="col-xs-12 col-sm-3">
								<div class="formazione--image">
									<?php
									if ( has_post_thumbnail() ){
										the_post_thumbnail( 'formazione-thumb', array('class' => 'img-responsive') );
									} else{
										echo '<img src="'.get_stylesheet_directory_uri().'/assets/images/placeholder-events.jpg" alt="placeholder immagine evento">';
									}?>
								</div>
								<div class="formazione--text">
									<h5><?php the_title(); ?></h5>
									<p><?php the_excerpt(); ?></p>
								</div>
							</div>
						</a>
	        <?php endwhile; ?>
			</section>
		<?php
		endif;
		wp_reset_postdata();
		//END Latest post?>



<div class="<?php echo esc_attr( hestia_layout() ); ?>">
<?php get_footer(); ?>
