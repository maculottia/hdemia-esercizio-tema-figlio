<?php
/**
 * Enqueue Parent styles and my custom scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}



// Aggiungere dimensioni personalizzate per il nostro tema
function wpse_setup_theme() {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'events-thumb', 300, 426, true ); //(cropped)
	add_image_size( 'formazione-thumb', 576, 378, true ); //(cropped)
}
add_action( 'after_setup_theme', 'wpse_setup_theme' );

// Modifica l'excerpt del template per mostrare solamente '...' anziché il link
function pm_excerpt( $more ){
  return '...';
}
add_filter('excerpt_more', 'pm_excerpt', 999 );


function cambioParole( $content ){
  str_replace('Daniele', 'Daniele Capelli', $content);
}
add_filter('the_content', 'cambioParole')


// Changing excerpt more
// function new_excerpt_more($more) {
//   global $post;
//   remove_filter('excerpt_more', 'new_excerpt_more');
//   return '';
// }
// add_filter('excerpt_more','new_excerpt_more',11);

?>
