<?php
/**
 * The template for displaying all single posts and attachments.
 *
 * @package Hestia
 * @since Hestia 1.0
 */

get_header();

do_action( 'hestia_before_single_post_wrapper' );
?>

<div class="<?php echo hestia_layout(); ?>">
	<section class="container vertical-spacing">
		<?php
		// Vars
		$imgArtist = get_field('img_artista');
		$nickname = get_field('nickname');
		$name = get_field('nome');
		$lastname = get_field('cognome');
		$isArtistActive = get_field('is_artist_active');
		$genere = get_field('genere');
		$albumType = get_field('album_type');
		?>

		<div class="row">
			<div class="col-md-4">
				<img src="<?php echo $imgArtist['sizes']['large']; ?>" alt="<?php $imgArtist['alt']; ?>" class="img-responsive">
			</div>
			<div class="col-md-8">
				<strong>
					<?php if ( $nickname ){ the_field('nickname'); }else{ echo $name . " " . $lastname; } ?>
				 </strong>
				 <h4>Biografia:</h4>
 	 		 	 <?php the_field('wyswyg_bio'); ?>
			</div>
		</div>


		<div class="row vertical-spacing">
			<div class="col-xs-12">
				<ul>
					<li> <b>Nome:</b> <?php the_field('nome'); ?>  </li>
					<li> <b>Cognome:</b> <?php the_field('cognome'); ?> </li>
					<li>
						<b>Periodo di attività musicale:</b> <?php the_field('start_date'); ?>
						-
						<?php
						if ( $isArtistActive ) {
							echo " in attività";
						} else{
							the_field('end_date');
						}
						?>
					</li>
					<?php if ( !empty($genere) ): ?>
						<li> <b>Genere:</b> <?php echo $genere; ?></li>
					<?php endif; ?>
					<li> <b>Album: </b> <?php the_field('album'); ?></li>
					<?php if( $albumType ): ?>
						<li>
							<b> <?php echo implode( ', ', $albumType ); ?> </b> : <?php the_field('album'); ?>
						</li>
					<?php endif;  ?>
				</ul>
			</div>
		</div>

	</section>



	<div class="blog-post blog-post-wrapper">
		<div class="container">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', 'single' );
				endwhile;
				else :
					get_template_part( 'template-parts/content', 'none' );
			endif;
				?>
		</div>
	</div>
</div>

<?php
if ( ! is_singular( 'elementor_library' ) ) {
	do_action( 'hestia_blog_related_posts' );
}
?>
<div class="footer-wrapper">
	<?php get_footer(); ?>
